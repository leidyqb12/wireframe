var secciones = [];
var tiempo_splash = 3500;
var regresivo = 20;
var num1 = 0;
var num2 = 0;
var operacion;
var puntaje = 0;
var relog;
var pez = 1;
var musica =0;
var letrero_correcto;
var comida_respuesta;
var texto_respuesta;
var puntaje_conteo;
var pez_imagen;
var tablanumeros;
var pez_objeto;
var comida;
var perdistebox;
var musica_objeto;
var cuentaregresiva;

window.onload = function(){
    inicializarReferencias();
    setTimeout(cambiarSplash,tiempo_splash);
    clearTimeout();
    ocultarpuntaje()
}

function ocultarpuntaje(){

    document.getElementById('puntaje').style.display = 'none';
    document.getElementById('tablanumeros').style.display = 'none';
    document.getElementById('pez').style.display = 'none';
    document.getElementById('comida').style.display = 'none';
    document.getElementById('correcto').style.visibility = 'hidden';
}

function inicializarReferencias(){
    secciones[1] = document.getElementById("splash_pantalla");
    secciones[2] = document.getElementById("menu_pantalla");
    secciones[3] = document.getElementById("instrucciones_pantalla");
    secciones[4] = document.getElementById("juego_pantalla");
    secciones[5] = document.getElementById("perdiste_pantalla");
    secciones[6] = document.getElementById("pausa_pantalla");
    secciones[7] = document.getElementById("creditos_pantalla");
    letrero_correcto = document.getElementById('correcto');
    comida_respuesta = document.getElementById('comida_respuesta');
    texto_respuesta = document.getElementById('texto_respuesta');
    puntaje_conteo = document.getElementById('puntaje');
    pez_imagen = document.getElementById('pez_imagen');
    tablanumeros = document.getElementById('tablanumeros');
    pez_objeto = document.getElementById('pez');
    comida = document.getElementById('comida');
    perdistebox = document.getElementById('perdistebox');
    musica_objeto = document.getElementById('musica');
    cuentaregresiva = document.getElementById('cuentaregresiva');
}

function cambiarSplash(){
    secciones[1].className = "splash oculto";
    secciones[2].className = "pantalla_menu";
}

function cambiarSeccion(id_seccion){

    for (var i in secciones) {
        secciones[i].classList.add("oculto");
    }

    secciones[id_seccion].classList.add("animated");
    secciones[id_seccion].classList.add("headShake");
    secciones[id_seccion].classList.remove("oculto");
    
    if(id_seccion==4)
    {
        letrero_correcto.style.visibility = 'hidden';
        comida_respuesta.src="img/comida.png";
        texto_respuesta.style.color = '#00a2ac';
        tiempo();
        suma_aleatroia();
    }
    else if(id_seccion == 6){
        clearInterval(relog);
    }
    else if(id_seccion == 2){
        regresivo = 20;
        clearInterval(relog);
        puntaje = 0;
        puntaje_conteo.innerHTML='Puntaje: '+puntaje;
        pez = 1;
        pez_imagen.src="img/1.png"
    }
}

function tiempo(){
    relog = setInterval(function(){
        if(regresivo <= 0){
            perder();
        } else {
            cuentaregresiva.innerHTML = regresivo +"&#39;";
            mostrarpuntaje();
        }
        regresivo -= 1;
        
    }, 1000);
}

function parar_tiempo(){
    clearInterval(relog);
}

function mostrarpuntaje(){
    document.getElementById('puntaje').style.display = 'block';
    tablanumeros.style.display = 'block';
    pez_objeto.style.display = 'block';
    comida.style.display = 'block';

}

function escribir(id_input){ 
    if(id_input == 10){
        borrar();
    }
    else if(id_input == 11){
        ok();
    }
    else{
        comida_respuesta.src="img/respuesta.png";
        texto_respuesta.style.color = '#ed6544';
        if(texto_respuesta.innerHTML == (num1 + "+" + num2)){
            texto_respuesta.innerHTML = id_input;
            texto_respuesta.style.left = "30%";
        }
        else{
            texto_respuesta.innerHTML += id_input;
            texto_respuesta.style.left = "22%";
        }
    }
}

function ok(){
    if(texto_respuesta.innerHTML != (num1 + "+" + num2)){
        operacion = document.getElementById('texto_respuesta').innerHTML;
        if(parseInt(operacion)==(num1+num2)){
            correcto();
        }
        else{
            incorrecto();
        }
        dormir(2000).then( ()=> comida_respuesta.src="img/comida.png");
        dormir(2000).then( ()=> texto_respuesta.style.color = '#00a2ac');
    }  
}
function borrar(){
    texto_respuesta.style.left = "15%";
    texto_respuesta.innerHTML= num1+'+'+num2;
    comida_respuesta.src="img/comida.png";
    texto_respuesta.style.color = '#00a2ac';
}

function suma_aleatroia(){
    texto_respuesta.style.left = "15%";
    num1 = Math.round(Math.random()*8 +1);
    num2 = Math.round(Math.random()*8 +1);
    texto_respuesta.innerHTML= num1+'+'+num2;
}

function sumar_puntaje(){
    puntaje += 1;
    puntaje_conteo.innerHTML= puntaje+' puntos';
}

function correcto(){
    letrero_correcto.innerHTML= 'correcto';
    letrero_correcto.style.visibility = 'visible';
    restar_tiempo();
    sumar_puntaje();
    dormir(2000).then( ()=> letrero_correcto.style.visibility = 'hidden');
    if(pez < 17){
        pez += 1;
    }
    animacion();
    clearInterval(relog);
    dormir(2000).then( ()=> suma_aleatroia());
    dormir(2000).then( ()=> pez_imagen.src="img/"+pez+".png");
    dormir(1600).then( ()=> tiempo());
}
function incorrecto(){
    perder();
}

function restar_tiempo(){
    if(puntaje < 10){
        regresivo = 20;
    }
    else if(puntaje < 20){
        regresivo = 15;
    }
    else if(puntaje < 30){
        regresivo = 10;
    }
    else if(puntaje < 40){
        regresivo = 5;
    }
    else{
        regresivo = 2;
    }
}

function dormir(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function perder(){
    letrero_correcto.innerHTML= 'incorrecto';
    letrero_correcto.style.visibility = 'visible';
    clearInterval(relog);
    dormir(2000).then( ()=> cambiarSeccion(5));
    perdistebox.innerHTML='Puntaje: '+puntaje;
    regresivo = 20;
    puntaje = 0;
    puntaje_conteo.innerHTML='Puntaje: '+puntaje;
    pez = 1;
    pez_imagen.src="img/1.png"
}

function animacion(){
    texto_respuesta.classList.add("texto_animado");
    comida_respuesta.classList.add("imagen_animada");
    dormir(2000).then( ()=> texto_respuesta.classList.remove("texto_animado"));
    dormir(2000).then( ()=> comida_respuesta.classList.remove("imagen_animada"));
}

function pausarmusica(){

    if(musica==0){
        musica_objeto.pause();
        musica=musica+1;
    }
    else{
        musica_objeto.play();
        musica=0;
    }
}

